function Window( theCaption, theId ){
	
	this.base;
	this.titleBar;
	this.menuBar;
	
	this.caption = theCaption;
	this.id = theId; // required for interaction with other scripts.
	
	this.minimized = false;
	this.maximized = false;
	
	this.offsets;
	
	this.hasOwnStyleSheet = false;
	
	var handle = this;
	
	this.show = function(){
		document.body.appendChild(this.base);
	}
	
	this.makeButtons = function(){
		var theButtons = document.createElement("span");
		
		var btnCss = {
			"border":"1px solid #999",
			"width":"23px",
			"height":"23px",
			"margin-left":"3px",
			"margin-top":"2px"
		};
		
		this.createStyleSheet(".btnCss", btnCss);
		
		
		// This is the Minimize Button //
		var minBtn = document.createElement("input");
		minBtn.type = "button";
		minBtn.value = "_";
		minBtn.className = "btnCss";
		minBtn.onclick = function(){
			if(!handle.minimized){
				handle.base.style.height = "30px";
				
				handle.minimized = true;
			} else {
				handle.base.style.height = "500px";
				
				handle.minimized = false;
			}
		}
		// END Minimize Button //
		
		// This is the Maximize Button //
		var maxBtn = document.createElement("input");
		maxBtn.type = "button";
		maxBtn.value = "[]";
		maxBtn.className = "btnCss";
		maxBtn.onclick = function(){
			if(!handle.maximized){
				
				handle.offsets = {
					"left": handle.base.style.left,
					"top": handle.base.style.top
				};
				
				handle.base.style.position = "absolute";
				handle.base.style.zIndex = "99";
				handle.base.style.left = "0";
				handle.base.style.top = "0";
				handle.base.style.width = "100%";
				
				
				
				
				// Cross-Browser way of getting the docHeight.
				// http://james.padolsey.com/javascript/get-document-height-cross-browser/
				
				var D = document;
				var docHeight = Math.max(
						Math.max(D.body.scrollHeight, D.documentElement.scrollHeight),
						Math.max(D.body.offsetHeight, D.documentElement.offsetHeight),
						Math.max(D.body.clientHeight, D.documentElement.clientHeight)
					);

					
				handle.base.style.height = docHeight + "px";
				
				
				//Handle Repainting the fileuploader
				if(handle.hasFileUploader){
					
					var parentHeight = document.getElementById(handle.id).offsetHeight;
					
					handle.fu.style.height = (parentHeight - 58) + "px"; 
				}
				
				handle.maximized = true;
			} else {
				handle.base.style.width = "300px";
				handle.base.style.height = "500px";
				
				handle.base.style.left = handle.offsets.left;
				handle.base.style.top = handle.offsets.top;
				
				
				//Handle Repainting the fileuploader
				if(handle.hasFileUploader){
					
					var parentHeight = document.getElementById(handle.id).offsetHeight;
					
					handle.fu.style.height = (parentHeight - 58) + "px"; 
				}
				
				
				handle.maximized = false;
			}
		}
		// END Maximize Button //
		
		// This is the Close Button //
		var closeBtn = document.createElement("input");
		closeBtn.type = "button";
		closeBtn.value = "X";
		closeBtn.className = "btnCss";
		closeBtn.onclick = function(){
			document.body.removeChild(document.getElementById(handle.id));
		}
		// END Close Button //
		
		theButtons.appendChild(minBtn);
		theButtons.appendChild(maxBtn);
		theButtons.appendChild(closeBtn);
		
		
		theButtons.style.position = "absolute";
		theButtons.style.right = "2px";
		theButtons.style.marginRight = "2px";
		theButtons.style.marginTop = "2px";
		
		return theButtons;
	}
	
	this.makeTitleBar = function(caption){
	
		var titleBar = document.createElement("span");
		
		var captionBox = document.createElement("span");
		var theCaption = document.createTextNode(caption);
		
		captionBox.appendChild(theCaption);
		
		captionBox.style.fontFamily = "'Trebuchet MS','Arial', sans-serif";
		captionBox.style.marginLeft = "5px";
		captionBox.style.lineHeight = "30px";
		
		titleBar.style.display = "block";
		titleBar.style.width = "100%";
		titleBar.style.height = "30px";
		titleBar.style.borderBottom = "1px solid #999";
		titleBar.style.backgroundColor = "#BC4242";
		
		titleBar.appendChild(captionBox);
		
		titleBar.appendChild(this.makeButtons());
		
		this.titleBar = titleBar;
	}
	
	this.makeMenuBar = function(){
		
		var menuStyle = {
			"float":"left",
			"list-style-type":"none",
			"padding-left":"0",
			"margin-top":"2px",
			"margin-left":"3px",
			"position":"relative",
			"width":"35px",
			"z-index":99
		};
		this.createStyleSheet(".menu", menuStyle);
		
		var menuItemStyle = {
			"padding":"2px 0 2px 0",
			"display":"block",
			"text-align":"center",
			"background":"#999",
			"margin-left":"-3px",
			"cursor":"pointer",
			"width":"75px",
			"color":"white",
			"position":"relative",
			"z-index":99
		};
		this.createStyleSheet(".menu:hover > .menuItem", menuItemStyle);
		
		menuItemStyle.display = "none";
		this.createStyleSheet(".menu > .menuItem", menuItemStyle);
		this.createStyleSheet(".menu:hover > .menuItem:hover", {"background":"#CCC"});
		this.createStyleSheet(".menu:hover > .menuItem:first-child", {"margin-top":"4px"});
		
		var menuBar = document.createElement("span");
		
		var mi = ["New","Open","Save","Save As...","Exit"];
		
		menuBar.addMenu = function( menuBar, text, menuItems ){
			
			var menu = document.createElement("ul");
			menu.className = "menu";
			menu.appendChild( document.createTextNode(text) );
			
			for(var i=0; i < menuItems.length; i++){
				var menuItem = document.createElement("li");
				menuItem.className = "menuItem";
				menuItem.appendChild(document.createTextNode( menuItems[i] ));
				menu.appendChild(menuItem);
			}
			
			menuBar.appendChild( menu );
		}
		
		menuBar.style.display = "block";
		menuBar.style.width = "100%";
		menuBar.style.height = "25px";
		menuBar.style.borderBottom = "1px solid #999";
		
		menuBar.addMenu(menuBar, "File", mi);
		menuBar.addMenu(menuBar, "Help", ["About","Services"]);
		
		this.menuBar = menuBar;
	}
	
	this.createFileUploader = function( urlDestination ){ // Not Complete.
	
	/************************************
	 * 
	 *  This Function is designed to add a "fileUploader" control to the window. 
	 *  The File uploader sends the file to the page that is specified in the argument.
	 * 
	 *  TO-DO: Change the submit button into a regular button, and process via Ajax.
	 * 		   Style the uploader
	 * 
	 * 
	 *
	 ************************************/
	
		try{
		
			var fu_panel = document.createElement("div");
			fu_panel.id= "fu_panel";
			
			var parentHeight = document.getElementById(handle.id).offsetHeight;
			
			fu_panel.style.width = "100%";
			fu_panel.style.height = (parentHeight - 58) + "px";
			fu_panel.style.position = "relative";
			
			var fu_folderListPanel = document.createElement("div");
			fu_folderListPanel.id = "fu_folderListPanel";
			fu_folderListPanel.style.position = "absolute";
			fu_folderListPanel.style.width = "20%";
			fu_folderListPanel.style.height = "100%";
			fu_folderListPanel.style.borderRight = "1px dotted #999";
			
			var fu_fileListPanel = document.createElement("div");
			fu_fileListPanel.id = "fu_fileListPanel";
			fu_fileListPanel.style.position = "absolute";
			fu_fileListPanel.style.width = "80%";
			fu_fileListPanel.style.height = "80%";
			fu_fileListPanel.style.top = "0";
			fu_fileListPanel.style.right = "0";
			fu_fileListPanel.style.borderBottom = "1px dotted #999";
			
			var fu_form = document.createElement("form");
			fu_form.id = "fu_form";
			fu_form.action = urlDestination;
			fu_form.style.position = "absolute";
			fu_form.style.width = "80%";
			fu_form.style.minHeight = "20%";
			fu_form.style.bottom = "0";
			fu_form.style.right = "0";
			
			fu_form.onsubmit = function(){
				
				if(fu_chooseFile.value == "" || fu_chooseFile.value == null){
					//If there is no file selected, reject the submission.
					return false;
				} else {
					//else, submit the files.
					return true;
				}
				
			}
			
			
			
			
			var fu_chooseFile = document.createElement("input");
			fu_chooseFile.type = "file";
			fu_chooseFile.name = "fu_chooseFile";
			fu_chooseFile.id = "fu_chooseFile";
			
			
			var fu_submit = document.createElement("input");
			fu_submit.type = "submit";
			fu_submit.id = "fu_submit";
			
			
			fu_form.appendChild(fu_chooseFile);
			fu_form.appendChild(fu_submit);
			
			fu_panel.appendChild(fu_folderListPanel);
			fu_panel.appendChild(fu_fileListPanel);
			fu_panel.appendChild(fu_form);
			
			this.base.appendChild(fu_panel);
			this.hasFileUploader = true;
			this.fu = fu_panel;
			
		} catch (exc) {
			
			alert(exc); // catch errors so you can debug, albeit a little hackishly
			
		}
	}
	
	
	this.createStyleSheet = function( selector, json ){
		
		
		if(!this.hasOwnStyleSheet){
			
			
			
			this.hasOwnStyleSheet = true;
			this.stylesheet = document.createElement("style");
			this.stylesheet.type='text/css';
			this.stylesheet.id = this.id + "_stylesheet";
		} 
		
		var styleRules = selector+"{\n";
			for(rule in json){
				styleRules += rule +":"+json[rule]+";\n";
			}
			styleRules += "}\n";
			
			var styles = document.createTextNode(styleRules);
			
			if(navigator.appName == "Microsoft Internet Explorer"){
				this.stylesheet.styleSheet.cssText += styleRules;
			} else {
				this.stylesheet.appendChild(styles);
			}
			
		document.body.appendChild(this.stylesheet);
	}
	
	
	
	
	
	
	this.window = function(){
		try{
		
			var winBase = document.createElement("div");
			winBase.style.position = "relative";
			winBase.style.width = "500px";
			winBase.style.height = "400px";
			winBase.style.border = "1px solid #999";
			winBase.style.backgroundColor = "#FFF";
			winBase.style.fontFamily = "\"Trebuchet MS\",\"Arial\",\"sans-serif\"";
			winBase.id = this.id;
			
			
			
			this.makeTitleBar(this.caption);
			this.makeMenuBar();
			
			winBase.appendChild(this.titleBar);
			winBase.appendChild(this.menuBar);
			
			this.base = winBase;
			this.show();
		
		} catch(exc) {
			
			alert(exc);
		}
	}
	
	this.window();
}